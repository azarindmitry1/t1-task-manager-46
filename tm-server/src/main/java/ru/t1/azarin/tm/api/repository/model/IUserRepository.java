package ru.t1.azarin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    void clear();

    @Nullable
    List<User> findAll();

    @Nullable
    User findOneById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    void removeById(@NotNull String id);

}
