package ru.t1.azarin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.TaskDTO;
import ru.t1.azarin.tm.enumerated.Status;

import java.util.List;

public interface ITaskServiceDTO extends IUserOwnedServiceDTO<TaskDTO> {

    void changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

}