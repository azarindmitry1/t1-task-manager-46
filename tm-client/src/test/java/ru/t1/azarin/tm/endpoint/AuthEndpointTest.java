package ru.t1.azarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.azarin.tm.api.endpoint.IUserEndpoint;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.dto.request.user.*;
import ru.t1.azarin.tm.dto.response.user.UserLoginResponse;
import ru.t1.azarin.tm.dto.response.user.UserLogoutResponse;
import ru.t1.azarin.tm.dto.response.user.UserViewProfileResponse;
import ru.t1.azarin.tm.marker.IntegrationCategory;
import ru.t1.azarin.tm.service.PropertyService;

import java.sql.SQLException;

@Category(IntegrationCategory.class)
public final class AuthEndpointTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IUserEndpoint USER_ENDPOINT = IUserEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final String USER1_LOGIN = "user";

    @NotNull
    private static final String USER1_PASSWORD = "user";

    @NotNull
    private static final String ADMIN_LOGIN = "admin";

    @NotNull
    private static final String ADMIN_PASSWORD = "admin";

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @NotNull
    private final String testString = "TEST_STRING";

    @BeforeClass
    public static void setUp() throws SQLException {
        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest();
        registryRequest.setLogin(USER1_LOGIN);
        registryRequest.setPassword(USER1_PASSWORD);
        USER_ENDPOINT.registryResponse(registryRequest);
    }

    @AfterClass
    public static void tearDown() throws SQLException {
        @NotNull final UserLoginRequest loginRquest = new UserLoginRequest(ADMIN_LOGIN, ADMIN_PASSWORD);
        @Nullable final UserLoginResponse loginResponse = AUTH_ENDPOINT.login(loginRquest);

        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(loginResponse.getToken());
        removeRequest.setLogin(USER1_LOGIN);
        USER_ENDPOINT.removeResponse(removeRequest);
    }

    @Test
    public void login() throws SQLException {
        Assert.assertThrows(Exception.class, () ->
                AUTH_ENDPOINT.login(new UserLoginRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                AUTH_ENDPOINT.login(new UserLoginRequest(emptyString, emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                AUTH_ENDPOINT.login(new UserLoginRequest(nullString, nullString))
        );
        @NotNull final UserLoginRequest request = new UserLoginRequest(USER1_LOGIN, USER1_PASSWORD);
        @Nullable final UserLoginResponse response = AUTH_ENDPOINT.login(request);
        Assert.assertNotNull(response.getToken());
        Assert.assertTrue(response.getSuccess());
    }

    @Test
    public void logout() throws SQLException {
        Assert.assertThrows(Exception.class, () ->
                AUTH_ENDPOINT.logout(new UserLogoutRequest())
        );
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(USER1_LOGIN, USER1_PASSWORD);
        @Nullable final UserLoginResponse loginResponse = AUTH_ENDPOINT.login(loginRequest);
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(loginResponse.getToken());
        @Nullable final UserLogoutResponse logoutResponse = AUTH_ENDPOINT.logout(logoutRequest);
        Assert.assertNotNull(logoutResponse);
        Assert.assertThrows(Exception.class, () ->
                AUTH_ENDPOINT.logout(new UserLogoutRequest(testString))
        );
    }

    @Test
    public void viewProfile() throws SQLException {
        Assert.assertThrows(Exception.class, () ->
                AUTH_ENDPOINT.viewProfile(new UserViewProfileRequest())
        );
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(USER1_LOGIN, USER1_PASSWORD);
        @Nullable final UserLoginResponse loginResponse = AUTH_ENDPOINT.login(loginRequest);
        @NotNull final UserViewProfileRequest viewProfileRequest = new UserViewProfileRequest(loginResponse.getToken());
        @Nullable final UserViewProfileResponse viewProfileResponse = AUTH_ENDPOINT.viewProfile(viewProfileRequest);
        Assert.assertEquals(USER1_LOGIN, viewProfileResponse.getUser().getLogin());
    }

}
